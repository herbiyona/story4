from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import FormCourse
from .models import Course

# Create your views here.
def coursedetail(request, detail_id):
    try:
        course_detail = Course.objects.get(id=detail_id)
        context = {
            'course':course_detail
        }
        return render(request, 'coursedetail.html', context)
    except:
        return redirect('/story5/courseadded')

def delete(request, delete_id):
    Course.objects.filter(id=delete_id).delete()
    return redirect('story5:courseadded')

def courseadded(request):
    courses = Course.objects.all()
    context = {
        'page_title':'All Schedule',
        'courses':courses
    }
    return render(request, 'courseadded.html',context)       

def courseadd(request):
    form_course = FormCourse(request.POST or None)
    if request.method == 'POST':
        if form_course.is_valid():
            Course.objects.create(
                name = form_course.cleaned_data['name'],
                lecturer = form_course.cleaned_data['lecturer'],
                sks = form_course.cleaned_data['sks'],
                desc = form_course.cleaned_data['desc'],
                term = form_course.cleaned_data['term'],
                place = form_course.cleaned_data['place']
            )
            return HttpResponseRedirect("/story5/courseadded")
    context = {
        'page_title':'Create Schedule',
        'form_course':form_course
    }
    return render(request, 'courseadd.html',context)
