from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('coursedetail/P<int:detail_id>/', views.coursedetail, name='coursedetail'),
    path('delete/P<int:delete_id>/', views.delete, name='delete'),
    path('', views.courseadd, name='courseadd'),
    path('courseadded/', views.courseadded, name='courseadded'),
]
