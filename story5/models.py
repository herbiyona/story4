from django.db import models

# Create your models here.
class Course(models.Model):
    name = models.CharField(max_length=50)
    lecturer = models.CharField(max_length=50)
    sks = models.IntegerField()
    desc = models.TextField(max_length=200)
    term = models.CharField(max_length=15)
    place = models.CharField(max_length=30)

    def __str__(self):
        return "{}. {}".format(self.id, self.name)
