from django import forms
from .models import Course

class FormCourse(forms.Form):
    class Meta:
        model = Course
        fields = ['name', 'lecturer','sks','desc','term','place']
        widgets ={
            'name' : forms.TextInput(attrs={'class': 'form-control'}),
            'lecturer' : forms.TextInput(attrs={'class': 'form-control'}),
            'sks' : forms.NumberInput(attrs={'class': 'form-control'}),
            'desc' : forms.Select(attrs={'class': 'form-control'}),
            'term' : forms.Select(attrs={'class': 'form-control'}),
            'place' : forms.TextInput(attrs={'class': 'form-control'}),
        }

    error_messages = {
        'required' : 'Please input text'
    }

    name = forms.CharField(
        label='Course Name :', required=True,
        max_length=50,
        widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Insert course name'}))

    lecturer = forms.CharField(
        label='Lecturer :', required=True,
        max_length=50,
        widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Insert lecturer name'}))

    sks = forms.IntegerField(
        label='Number of Credits :', required=True,
        widget=forms.NumberInput(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Insert credits of this course'}))

    desc = forms.CharField(
        label='Description :', required=True,
        max_length=200,
        widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Write all about this course'}))

    term = forms.CharField(
        label='Term and Year :', required=True,
        max_length=15,
        widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Insert term and year (ex : Gasal 2019/2020)'}))

    place = forms.CharField(
        label='Location :', required=True,
        max_length=30,
        widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Where this class take place'}))
