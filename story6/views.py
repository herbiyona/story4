from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import FormActivities, FormPeople
from .models import People,Activities

# Create your views here.

def allactivities(request):
    activities = Activities.objects.all()
    people = People.objects.all()
    context = {
        'page_title':'All Activities',
        'activities':activities,
        'people':people,
    }
    return render(request, 'allactivities.html',context) 

def addactivities(request):
    form_activities = FormActivities(request.POST or None)
    if request.method == 'POST':
        if form_activities.is_valid():
            Activities.objects.create(
                name = form_activities.cleaned_data['name'],
            )
            return redirect("/story6/")
        else:
            return render(request, 'addactivities.html', {'form_activities':form_activities, 'status':'failed'})
    else:
        return render(request, 'addactivities.html', {'form_activities':form_activities})

def addparticipant(request, ppl_id):
    form_participant = FormPeople(request.POST or None)
    if request.method == 'POST':
        if form_participant.is_valid():
            People.objects.create(
                participant = form_participant.cleaned_data['participant'],
                activities = Activities.objects.get(id = ppl_id)
            )
            return redirect("/story6/")
        else:
            return render(request,'addparticipant.html', {'form_participant':form_participant, 'status':'failed'})
    else:
        return render(request, 'addparticipant.html', {'form_participant':form_participant})

