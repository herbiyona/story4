from django.db import models

# Create your models here.

class Activities(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class People(models.Model):
    participant = models.CharField(max_length=50)
    activities = models.ForeignKey(Activities, on_delete=models.CASCADE,blank=True,null=True)
    def __str__(self):
        return self.participant

