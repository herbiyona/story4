from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.allactivities, name='allactivities'),
    path('addactivities/', views.addactivities, name='addactivities'),
    path('addparticipant/P<int:ppl_id>/', views.addparticipant, name='addparticipant'),
]