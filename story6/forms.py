from django import forms
from .models import Activities, People

class FormActivities(forms.Form):
    class Meta:
        model = Activities
        fields = ['name']
        widgets ={
            'name' : forms.TextInput(attrs={'class': 'form-control'}),
        }

    error_messages = {
        'required' : 'Please input text'
    }

    name = forms.CharField(
        label='Activity Name :', required=True,
        max_length=100,
        widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Insert activity name'}))

class FormPeople(forms.Form):
    class Meta:
        model = People
        fields = ['participant']
        widgets ={
            'participant' : forms.TextInput(attrs={'class': 'form-control'}),
        }

    error_messages = {
        'required' : 'Please input text'
    }

    participant = forms.CharField(
        label='Name :', required=True,
        max_length=50,
        widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'type' : 'text',
            'placeholder' : 'Insert your name'}))
