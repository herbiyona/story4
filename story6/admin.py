from django.contrib import admin

# Register your models here.
from .models import Activities,People

admin.site.register(Activities)
admin.site.register(People)