from django.test import TestCase, Client
from django.urls import resolve,reverse
from django.apps import apps
from .models import Activities, People
from .views import addactivities, addparticipant, allactivities
from .forms import FormActivities, FormPeople
from .apps import Story6Config

# Create your tests here.
class ModelTest(TestCase):
    def setUp(self):
        self.name = Activities.objects.create(name="Ngoding")
        self.participant = People.objects.create(participant="Yona")

    def test_model_created(self):
        self.assertEqual(Activities.objects.all().count(), 1)
        self.assertEqual(People.objects.all().count(), 1)

    def test_model_fields(self):
        self.assertEqual(str(self.name), "Ngoding")
        self.assertEqual(str(self.participant), "Yona")

class UrlsTest(TestCase):
    def setUp(self):
        self.name = Activities.objects.create(name="Ngoding")
        self.participant = People.objects.create(participant="Yona", activities=Activities.objects.get(name="Ngoding"))
        self.allactivities = reverse('story6:allactivities')
        self.addactivities = reverse('story6:addactivities')
        self.addparticipant = reverse('story6:addparticipant', args=[self.participant.pk])

    def test_landing_page(self):
        found = resolve(self.allactivities)
        self.assertEqual(found.func, allactivities)

    def test_add_activities(self):
        found = resolve(self.addactivities)
        self.assertEqual(found.func, addactivities)

    def test_add_participant(self):
        found = resolve(self.addparticipant)
        self.assertEqual(found.func, addparticipant)

class ViewsTest(TestCase):
    def setUp(self):
        self.allactivities = reverse("story6:allactivities")
        self.addactivities = reverse("story6:addactivities")

    def test_allactivities_get(self):
        response = Client().get(self.allactivities)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'allactivities.html')

    def test_addactivities_get(self):
        response = Client().get(self.addactivities)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addactivities.html')
    
    def test_addactivities_post(self):
        response = Client().post(self.addactivities,{'name':'Ngoding'}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_addactivities_invalid_post(self):
        response = Client().post(self.addactivities,{'name':''}, follow=True)
        self.assertTemplateUsed(response, 'addactivities.html')

class FormTest(TestCase):
    def setUp(self):
        activities = Activities(name="baca")
        activities.save()
        
    def test_addparticipant_post(self):
        response = Client().post('/story6/addparticipant/P1/', data={'participant':'A'})
        self.assertEqual(response.status_code, 302)

    def test_addparticipant_get(self):
        response = Client().get('/story6/addparticipant/P1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addparticipant.html')

    def test_addparticipant_invalid_post(self):
        response = Client().post('/story6/addparticipant/P1/', data={'participant':''})
        self.assertTemplateUsed(response, 'addparticipant.html')

class AppTest(TestCase):
    def test_app(self):
        self.assertEqual(Story6Config.name, 'story6')