from django.shortcuts import render

def home(request):
    return render(request, 'main/home.html')

def aboutme(request):
    return render(request, 'main/aboutme.html')

def contactskill(request):
    return render(request, 'main/contactskill.html')

def experience(request):
    return render(request, 'main/experience.html')

def works(request):
    return render(request, 'main/works.html')

def story1(request):
    return render(request, 'main/story1.html')

