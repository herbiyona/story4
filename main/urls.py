from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('contactskill/', views.contactskill, name='contactskill'),
    path('experience/', views.experience, name='experience'),
    path('works/', views.works, name='works'),
    path('story1/', views.story1, name='story1'),
]
